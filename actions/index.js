import 'isomorphic-unfetch'

import * as types from '../constants'

// products
const receiveProducts = products => ({
  type: types.RECEIVE_PRODUCTS,
  products
})

const addProductToCart = productId => ({
  type: types.ADD_TO_CART,
  productId
})

export const getAllProducts = () => dispatch => {
  fetch('http://www.mocky.io/v2/5cdc87912d0000e1c0f5a7e5')
    .then(response => response.json())
    .then(data => data)
    .then(products => {
      dispatch(receiveProducts(products.Produtos))
    })
    .catch(err => {
      // Handle errors
      console.log(err)
    })
}

export const addToCart = productId => (dispatch) => {
  dispatch(addProductToCart(productId))
}

// menu
const receiveMenu = items => ({
  type: types.RECEIVE_MENU,
  items
})

export const getMenu = () => dispatch => {
  fetch('http://www.mocky.io/v2/5cdc82a32d0000b217f5a7d8')
    .then(response => response.json())
    .then(data => data)
    .then(items => {
      dispatch(receiveMenu(items.Departamentos))
    })
}

const toggleMenuItems = status => {
  return {
    type: types.TOGGLE_MENU,
    status
  }
}

export const toggleMenu = status => dispatch => {
  dispatch(toggleMenuItems({
    toggled: !status.toggled
  }))
}