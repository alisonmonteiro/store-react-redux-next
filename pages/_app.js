import App, { Container } from 'next/app'
import { Provider } from 'react-redux'

import withReduxStore from '../lib/with-redux-store'

class CustomApp extends App {
  render () {
    const { Component, pageProps, reduxStore } = this.props

    return (
      <Container>
        <Provider store={reduxStore}>
          <Component {...pageProps} />
        </Provider>

        <style jsx global>{`
          html, body {
            font-family: Helvetica, Arial, sans-serif;
            border: 0;
            margin: 0;
          }

          * {
            box-sizing: border-box;
          }
        `}</style>
      </Container>
    )
  }
}

export default withReduxStore(CustomApp)
