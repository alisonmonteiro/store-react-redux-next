import React from 'react'
import { connect } from 'react-redux'

import Header from '../containers/Header'
import ProductsContainer from '../containers/ProductsContainer'
import { getAllProducts, getMenu } from '../actions'

class Index extends React.Component {
  // Client side actions
  componentDidMount () {
    this.props.getMenu()
    this.props.getAllProducts()
  }

  render () {
    return (
      <div className="App">
        <Header />
        <ProductsContainer />

        <style>{`
          .App {
            width: 100%;
            max-width: 1100px;
            margin: 0 auto;
            background: #efefef;
          }
        `}</style>
      </div>
    )
  }
}

const mapDispatchToProps = { getAllProducts, getMenu }

export default connect(
  null,
  mapDispatchToProps
)(Index)
