import { combineReducers } from 'redux'
import { RECEIVE_MENU, TOGGLE_MENU } from '../constants'

const menuStatus = (state = {}, action) => {
  switch (action.type) {
    case TOGGLE_MENU:
      return {
        ...state,
        status: action.status
      }
    default:
      return {
        ...state,
        status: {
          toggled: false
        }
      }
  }
}

const visibleMenuItems = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_MENU:
      return action.items.filter(item => item.FlagMenu)
    default:
      return state
  }
}

export default combineReducers({
  menuStatus,
  visibleMenuItems
})

export const getVisibleMenuItems = state => (
  state.visibleMenuItems
)

export const getStatus = state => (
  state.menuStatus.status
)