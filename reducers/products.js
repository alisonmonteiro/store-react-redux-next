import { combineReducers } from 'redux'
import { RECEIVE_PRODUCTS, ADD_TO_CART } from '../constants'

const products = (state, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
      }
    default:
      return state
  }
}

const byId = (state = {}, action) => {
  const productId = action.productId

  switch (action.type) {
    case RECEIVE_PRODUCTS:
      return {
        ...state,
        ...action.products.reduce((obj, product) => {
          obj[product.IdProduto] = product
          return obj
        }, {})
      }

    case ADD_TO_CART:
      if (productId) {
        return {
          ...state,
          [productId]: products(state[productId], action)
        }
      }
      return state

    default:
      return state
  }
}

const visibleIds = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_PRODUCTS:
      return action.products.map(product => product.IdProduto)
    default:
      return state
  }
}

export default combineReducers({
  byId,
  visibleIds
})

export const getProduct = (state, id) => (
  state.byId[id]
)

export const getVisibleProducts = state => (
  state.visibleIds.map(id => getProduct(state, id))
)