import { combineReducers } from 'redux'

import products from './products'
import menu from './menu'

export default combineReducers({
  menu,
  products
})