import { connect } from 'react-redux'
import { IoIosSearch, IoIosHeart, IoMdCart } from 'react-icons/io'

import { toggleMenu } from '../actions'
import { getVisibleMenuItems, getStatus } from '../reducers/menu'
import Menu from '../components/Menu'

const Header = ({ menuItems, status, toggleMenu }) => (
  <header className="Header">
    <Menu items={menuItems} opened={status.toggled} toggler={() => toggleMenu(status)} />
    <div className="Header__icons">
      <button><IoIosSearch /></button>
      <button><IoIosHeart /></button>
      <button><IoMdCart /></button>
    </div>

    <style jsx>{`
      header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        background: #404040;
        padding: 6px;
      }

      .Header__icons button {
        color: #FFF;
        font-size: 30px;
        background: transparent;
        border: 0;
        -webkit-appearance: none;
      }
    `}</style>
  </header>
)

const mapStateToProps = state => ({
  menuItems: getVisibleMenuItems(state.menu),
  status: getStatus(state.menu)
})

export default connect(
  mapStateToProps,
  { toggleMenu }
)(Header)
