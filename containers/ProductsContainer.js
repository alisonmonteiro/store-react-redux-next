import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import ProductItem from '../components/ProductItem'
import ProductsList from '../components/ProductsList'
import { addToCart } from '../actions'
import { getVisibleProducts } from '../reducers/products'

const ProductsContainer = ({ products }) => (
  <div className="ProductsContainer">
    <ProductsList counter={products.length}>
      {products.map(product =>
        <ProductItem key={product.IdProduto} product={product} />
      )}
    </ProductsList>
  </div>
)

ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    IdProduto: PropTypes.number.isRequired,
    Nome: PropTypes.string.isRequired,
    Preco: PropTypes.object.isRequired,
  })).isRequired,
}

const mapStateToProps = state => ({
  products: getVisibleProducts(state.products)
})

export default connect(
  mapStateToProps,
  { addToCart }
)(ProductsContainer)