const MenuHamburger = ({ toggler }) => (
  <div className="Menu__hamburger" onClick={() => toggler()}>
    <span className="Menu__hamburger-icon"></span>

    <style scoped jsx>{`
      .Menu__hamburger {
        position: relative;
        height: 42px;
        width: 42px;
      }

      .Menu__hamburger-icon,
      .Menu__hamburger-icon:after,
      .Menu__hamburger-icon:before {
        display: block;
        position: absolute;
        width: 30px;
        height: 2px;
        background: #FFF;
      }

      .Menu__hamburger-icon:after,
      .Menu__hamburger-icon:before {
        content: '';
      }

      .Menu__hamburger-icon:before {
        top: -10px;
      }

      .Menu__hamburger-icon {
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
      }

      .Menu__hamburger-icon:after {
        top: 10px;
      }
    `}</style>
  </div>
)

export default MenuHamburger
