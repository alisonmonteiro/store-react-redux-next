import PropTypes from 'prop-types'
import { IoIosStar } from 'react-icons/io'

const maxRating = 5
const averages = Array.from(Array(maxRating), (x, index) => index + 1)

const AverageReview = ({ rating }) => (
  <div className="RatingList">
    {averages.map(number => (
      <IoIosStar key={number} color={number <= rating ? '#ffbf00' : '#bfbfbf'} />
    ))}

    <style jsx>{`
    .RatingList {
      display: flex;
    }
    `}</style>
  </div>
)

AverageReview.propTypes = {
  rating: PropTypes.number,
}

export default AverageReview