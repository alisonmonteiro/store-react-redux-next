import PropTypes from 'prop-types'

const ProductsList = ({ counter, children }) => (
  <div className="ProductsList">
    <div className="ProductsList__counter"><span>{counter}</span> encontrados</div>
    <div className="ProductsList__items">{children}</div>

    <style scoped>{`
      .ProductsList {
        padding: 10px;
      }

      .ProductsList__counter {
        color: #444;
        margin-bottom: 10px;
      }

      .ProductsList__counter span {
        color: #000;
        font-weight: bold;
      }

      .ProductsList__items {
        display: flex;
        flex-wrap: wrap;
        border: solid #dadada;
        border-width: 1px 0 0 1px;
      }
    `}</style>
  </div>
)

ProductsList.propTypes = {
  counter: PropTypes.number,
  children: PropTypes.node,
}

export default ProductsList
