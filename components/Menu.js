import PropTypes from 'prop-types'
import MenuItem from './MenuItem'
import MenuHamburger from './MenuHamburger'
import MenuHeading from './MenuHeading'

const Menu = ({ items, toggler, opened }) => {
  return (
    <div className="Menu">
      <MenuHamburger toggler={() => toggler()} />


      <div className={`MenuItems ${opened ? 'MenuItems--opened' : ''}`} onClick={() => toggler()}>
        <MenuHeading />

        <div className="MenuItems__content">
          <ul>
            <li className="MenuItem MenuItem--home">Home</li>
            <li className="MenuItem MenuItem--separator">Compre por departamentos</li>
            {items.map(item => {
              return <MenuItem key={item.IdDepartamento} item={item} />
            })}
          </ul>
          <div className="CtaButton">
            <button>Ver todos os departamentos</button>
          </div>
        </div>
      </div>

      <style scoped jsx>{`
      .CtaButton,
      .MenuItems {
        max-width: 90vw;
      }

      .MenuItems {
        position: absolute;
        left: 0;
        top: 0;
        background: #FFF;
        height: 100vh;
        width: 100%;
        opacity: 0;
        transform: translateX(-100%);
        box-shadow: 11px 0px 41px rgba(0, 0, 0, .5);
        transition: all .2s;
      }

      .MenuItems--opened {
        opacity: 1;
        transform: translateX(0);
      }

      .MenuItems__content {
        background: #FFF;
        height: calc(100vh - 90px);
        position: relative;
      }

      .MenuItems ul {
        list-style: none;
        padding: 0;
        margin: 0;
        height: calc(100vh - 165px);
        overflow: scroll;
      }

      .MenuItem--home {
        color: #404040;
        font-size: 16px;
        text-transform: uppercase;
        font-weight: bold;
        padding: 10px 20px;
      }

      .MenuItem--separator {
        background: #d2d2d2;
        font-size: 12px;
        text-transform: uppercase;
        padding: 8px 10px;
        color: #404040;
        margin-bottom: 10px;
      }

      .CtaButton {
        position: absolute;
        bottom: 0px;
        width: 100%;
        padding: 8px;
      }

      .CtaButton button {
        width: 100%;
        font-size: 15px;
        line-height: 48px;
        border: 1px solid #444;
        color: #444;
        -webkit-appearance: none;
        background: transparent;
      }
      `}</style>
    </div>
  )
}

Menu.propTypes = {
  items: PropTypes.array,
  opened: PropTypes.bool,
}

export default Menu