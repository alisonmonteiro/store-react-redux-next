import PropTypes from 'prop-types'

const Price = ({ value, installments, installmentsValue }) => (
  <div className="Price">
    <div className="Price__full">Por: R$ {value}</div>
    <div className="Price__installments">{installments ? `${installments}x de R$ ${installmentsValue}` : null}</div>

    <style jsx>{`
    .Price {
      font-size: 16px;
      text-align: right;
    }

    .Price__installments {
      font-size: 12px;
    }
    `}</style>
  </div>
)

Price.propTypes = {
  value: PropTypes.number,
  installments: PropTypes.number,
  installmentsValue: PropTypes.number,
}

export default Price