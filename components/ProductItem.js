import PropTypes from 'prop-types'
import Product from './Product'

const TITLE_LIMIT = 40

const ProductItem = ({ product, onAddToCartClicked }) => (
  <div className="ProductItem" onClick={onAddToCartClicked}>
    <Product
      title={(product.Nome.length > TITLE_LIMIT) ? `${product.Nome.substring(0, TITLE_LIMIT)}...` : product.Nome}
      rating={product.Avaliacao}
      image={product.Imagem}
      price={product.Preco} />

      <style jsx>{`
        .ProductItem {
          width: 50%;
          padding: 16px;
          border-style: solid;
          border-color: #dadada;
          border-width: 0 1px 1px 0;
          border-collapse: collapse;
          background: #fff;
        }

        @media (min-width: 769px) {
          .ProductItem {
            width: calc(100% / 3)
          }
        }
      `}</style>
  </div>
)

ProductItem.propTypes = {
  product: PropTypes.shape({
    Nome: PropTypes.string.isRequired,
    Imagem: PropTypes.string.isRequired,
    Avaliacao: PropTypes.number,
  }).isRequired,
  onAddToCartClicked: PropTypes.func
}

export default ProductItem