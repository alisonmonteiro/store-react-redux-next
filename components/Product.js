import PropTypes from 'prop-types'
import Price from './Price'
import AverageReview from './AverageReview'

const Product = ({ image, title, price, rating }) => (
  <div className="Product">
    <figure className="Product__figure">
      <img src={image} alt={title} />
    </figure>
    <div>
      <h3 className="Product__title">{title}</h3>
      <div className="Product__detail">
        <AverageReview rating={rating} />
        <Price value={price.Por} installments={price.Parcelamento} installmentsValue={price.PorParcela} />
      </div>
    </div>

    <style>{`
    .Product__figure {
      margin: 0;
      padding: 0;
    }

    .Product__figure img {
      width: auto;
      max-width: 100%;
    }

    .Product__title {
      font-size: 14px;
      font-weight: 400;
    }

    .RatingList {
      margin-bottom: 8px;
    }
    `}</style>
  </div>
)

Product.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  rating: PropTypes.number
}

export default Product
