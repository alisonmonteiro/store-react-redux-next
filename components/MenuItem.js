import PropTypes from 'prop-types'

const MenuItem = ({ item }) => (
  <li className="MenuItem">
    {item.Nome}

    <style jsx>{`
      .MenuItem {
        color: #929292;
        font-size: 16px;
        text-transform: uppercase;
        font-weight: bold;
        padding: 10px 20px;
      }
    `}</style>
  </li>
)

MenuItem.propTypes = {
  item: PropTypes.object.isRequired,
}

export default MenuItem