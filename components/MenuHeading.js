import { IoIosClose, IoIosSettings, IoIosCart } from 'react-icons/io'

const MenuHeading = () => (
  <div className="MenuItems__heading">
    <div className="Heading__user-info">
      <IoIosClose className="IoIosClose" />
      <div className="Heading__user-name">Olá, Visitante</div>
      <button className="Heading__user-signup">cadastre-se</button>
    </div>
    <div>
      <div className="HeadingAccount">
        <div className="HeadingAccount__item">
          <IoIosCart /> <span>Meus pedidos</span>
        </div>
        <div className="HeadingAccount__item">
          <IoIosSettings /> <span>Minha conta</span>
        </div>
      </div>
    </div>

    <style>{`
    .MenuItems__heading {
      padding-bottom: 20px;
      background: #404040;
    }

    .IoIosClose {
      color: #FFF;
      font-size: 50px;
    }

    .Heading__user-info,
    .HeadingAccount {
      display: flex;
      align-items: center;
    }

    .Heading__user-name {
      color: #FFF;
      flex: 1;
      text-transform: uppercase;
      font-size: 13px;
      font-weight: bold;
    }

    .Heading__user-signup {
      background: transparent;
      border: 0;
      color: #e0e0e0;
      font-size: 13px;
      margin-right: 10px;
      -webkit-appearance: none;
    }

    .HeadingAccount {
      color: #FFF;
      justify-content: space-between;
      padding-left: 50px;
      padding-right: 16px;
    }

    .HeadingAccount__item {
      display: flex;
      align-items: center;
      font-size: 20px;
    }

    .HeadingAccount span {
      font-size: 12px;
      margin-left: 8px;
    }
    `}</style>
  </div>
)

export default MenuHeading